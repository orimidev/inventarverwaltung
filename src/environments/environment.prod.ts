export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyBkC0PyEF2SfjTJRG_evWYdHkXgMrsnjwQ',
    authDomain: 'inventarverwaltung-a41b3.firebaseapp.com',
    databaseURL: 'https://inventarverwaltung-a41b3.firebaseio.com',
    projectId: 'inventarverwaltung-a41b3',
    storageBucket: 'inventarverwaltung-a41b3.appspot.com',
    messagingSenderId: '948288574413',
    appId: '1:948288574413:web:a790dbaa1fafafdddfea84',
    measurementId: 'G-28WJ4GMMWW',
  },
};
