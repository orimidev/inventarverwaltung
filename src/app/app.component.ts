import { Component, OnInit } from '@angular/core';
import { MatSelectChange } from '@angular/material/select';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { skipWhile, take } from 'rxjs/operators';
import { AuthService } from './services/backend/auth/auth.service';
import { CurrentStateService } from './services/current-state.service';
import { HeaderService } from './services/header.service';
import { PopupService } from './services/popup.service';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  public title = 'inventarverwaltung-webapp';

  constructor(
    public router: Router,
    public route: ActivatedRoute,
    public header: HeaderService,
    private popupService: PopupService,
    public current: CurrentStateService,
    public authService: AuthService,
    public userService: UserService,
    public translate: TranslateService
  ) {}

  public ngOnInit(): void {
    this.userService.initialzed$
      .pipe(
        skipWhile((value) => value === false),
        take(1)
      )
      .subscribe((value) => {
        this.current.loading = !value;
        this.router.navigate(['home']);
      });
  }

  public goTo(route: string): void {
    this.router.navigate(['/' + route]);
    this.header.navBarOpend = false;
  }

  public isActivatedRoute(path: string): string {
    return this.router.isActive(path, false) ? 'primary' : 'accent';
  }

  public signOut(): void {
    this.userService.currentUser = undefined;
    this.authService.signOut();
    this.goTo('home');
  }

  public onRoleChange(event: MatSelectChange): void {
    this.userService.currentUser.role = event.value;
  }

  public openFilter(): void {
    this.popupService.openSuperSearchPopup();
  }
}
