import { Component, Inject, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { UserRoleEnum } from 'src/app/models/roles.enum';
import { CurrentStateService } from 'src/app/services/current-state.service';
import { HeaderService } from 'src/app/services/header.service';
import { UserRoleModel } from '../../../models/role.model';
import { AuthService } from '../../../services/backend/auth/auth.service';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-regis-popup',
  templateUrl: './regis-popup.component.html',
})
export class RegisPopupComponent implements OnInit {
  public regisForm: FormGroup;
  public showButtonLoading: boolean;

  constructor(
    public current: CurrentStateService,
    public header: HeaderService,
    public translate: TranslateService,
    public dialogRef: MatDialogRef<RegisPopupComponent>,
    public authService: AuthService,
    public userService: UserService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fg: FormBuilder
  ) {
    this.regisForm = this.fg.group({
      text: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
      fullname: new FormControl('', [Validators.required]),
    });
  }

  public ngOnInit() {}

  public tryRegis(): void {
    this.authService
      .signUp(this.regisForm.value.text, this.regisForm.value.password)
      .then(
        (res) => {
          const newRole = new UserRoleModel(
            res.user.uid,
            this.regisForm.value.text,
            this.regisForm.value.fullname,
            UserRoleEnum.Employee
          );
          this.userService
            .addNewUser(newRole)
            .then(() => {
              this.dialogRef.close({ success: true });
            })
            .catch((e) => {
              this.dialogRef.close({ success: false, error: e });
            });
          this.showButtonLoading = false;
        },
        (err) => {
          this.showButtonLoading = false;
          this.dialogRef.close({ success: false, error: err });
        }
      );
  }
}
