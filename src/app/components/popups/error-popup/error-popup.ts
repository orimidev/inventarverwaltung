import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-error-popup',
  templateUrl: 'error-popup.html',
  styleUrls: ['error-popup.scss'],
})
export class ErrorPopupComponent {
  constructor(
    public dialogRef: MatDialogRef<ErrorPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  public closePopup(): void {
    this.dialogRef.close();
  }
}
