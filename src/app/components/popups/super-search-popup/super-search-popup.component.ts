import { Component, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { takeWhile } from 'rxjs/operators';
import { StorageModel } from 'src/app/models/storage.model';
import { FurnitureGroup } from '../../../models/furnitureGroup.model';
import { FurnitureGroupService } from '../../../services/furniture-group.service';
import { PopupService } from '../../../services/popup.service';

@Component({
  selector: 'app-super-search-popup',
  templateUrl: 'super-search-popup.component.html',
  styleUrls: ['super-search-popup.component.scss'],
})
export class SuperSearchPopupComponent implements OnDestroy {
  public groups: FurnitureGroup[] = [];
  public allGroups: FurnitureGroup[];

  /**
   * unsubscribe
   */
  private alive = true;

  constructor(
    public dialogRef: MatDialogRef<SuperSearchPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public popupService: PopupService,
    public itemsGroupService: FurnitureGroupService
  ) {
    this.getAllGroupsFromService();
  }

  public getAllGroupsFromService() {
    this.itemsGroupService.furnitures$
      .pipe(takeWhile(() => this.alive))
      .subscribe((d) => {
        this.allGroups = d;
      });
  }

  public onItemClick(event: FurnitureGroup) {
    if (this.itemsGroupService.getTotalStockCount(event) > 0) {
      this.popupService.openItemReservationPopup(event);
    } else {
      this.popupService.openSnackbarWithText(
        `'${event.name}' ist zur Zeit leider nicht verfügbar`
      );
    }
  }

  public ngOnDestroy(): void {
    this.alive = false;
  }

  public getStorageNames(storages: StorageModel[]): string {
    let stores = '';
    for (const store of storages) {
      stores += `${store.name}, `;
    }
    return stores;
  }

  public applyFilter(filterValue: string): void {
    const val = this.normalizeString(filterValue);
    this.groups = [];
    this.allGroups.forEach((d) => {
      if (
        this.normalizeString(d.name).includes(val) ||
        this.normalizeString(d.measurements).includes(val) ||
        this.normalizeString(d.description).includes(val) ||
        this.normalizeString(d.color.colorName).includes(val) ||
        this.normalizeString(d.partnumber).includes(val)
      ) {
        this.groups.push(d);
      }
    });
  }

  public normalizeString(text: string): string {
    return text
      .toLowerCase()
      .replace(/ /g, '')
      .split('.')
      .join('');
  }
}
