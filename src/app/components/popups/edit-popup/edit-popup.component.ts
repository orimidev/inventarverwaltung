import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ImageUploadService } from 'src/app/services/image-upload.service';
import { isNullOrUndefined } from 'util';
import { Color } from '../../../models/color.model';
import { FurnitureCategory } from '../../../models/furnitureCategory.model';
import { ColorService } from '../../../services/backend/color.service';
import { FurnitureCategoryService } from '../../../services/furniture-category.service';
import { FurnitureGroupService } from '../../../services/furniture-group.service';
import { StorageRoomService } from '../../../services/storage.service';
import { FurnitureGroup } from './../../../models/furnitureGroup.model';
import { StorageModel } from './../../../models/storage.model';

@Component({
  selector: 'app-edit-popup',
  templateUrl: './edit-popup.component.html',
})
export class EditPopupComponent implements OnInit {
  public typeOfEdit: number;

  public groupForm: FormGroup;
  public categoryForm: FormGroup;
  public storageForm: FormGroup;
  public colorForm: FormGroup;
  public newImage: string;

  public imageCategoryPreviewPath: string;
  public imagePreviewPath: string;

  /**
   * unsubscribe
   */
  private alive = true;

  constructor(
    public dialogRef: MatDialogRef<EditPopupComponent>,
    public storageRoomService: StorageRoomService,
    public categoryService: FurnitureCategoryService,
    public groupService: FurnitureGroupService,
    public colorService: ColorService,
    public uploadService: ImageUploadService,
    private fg: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.typeOfEdit = data.type;
    this.prepairFormgroup();
  }

  public ngOnInit() {}

  /**
   *
   * @param type => 1: group ; 2: category; 3: room, 4: color
   */
  public submit(type: number): void {
    switch (type) {
      case 1:
        let group: FurnitureGroup = this.groupForm.value;
        group.furnitureGroupId = this.data.item.furnitureGroupId;
        if (!isNullOrUndefined(this.newImage)) {
          group = { ...this.groupForm.value, ...{ imgPath: this.newImage } };
        }
        this.groupService.update(group).then((res) => {
          this.dialogRef.close({
            success: true,
            name: 'Möbelstück',
          });
        });
        break;
      case 2:
        let cat: FurnitureCategory = this.categoryForm.value;
        cat.furnitureCategoryId = this.data.item.furnitureCategoryId;

        if (!isNullOrUndefined(this.newImage)) {
          cat = { ...this.categoryForm.value, ...{ imgPath: this.newImage } };
        }

        this.categoryService.update(cat).then((res) => {
          this.dialogRef.close({
            success: true,
            name: 'Kategory',
          });
        });
        break;
      case 3:
        const store: StorageModel = this.storageForm.value;
        store.id = this.data.item.id;
        this.storageRoomService.update(store).then((res) => {
          for (const forGroup of this.groupService.furnitures) {
            const indexOfStoreToUpdate = forGroup.storages.findIndex(
              (x) => x.id === this.data.item.id
            );

            const newGroupData = forGroup;
            newGroupData.storages[indexOfStoreToUpdate].name = store.name;
            this.groupService.update(newGroupData);
          }
          this.dialogRef.close({
            success: true,
            name: 'Lagerort',
          });
        });
        break;

      case 4:
        const color: Color = this.colorForm.value;
        color.colorId = this.data.item.colorId;
        this.colorService.update(color).then((res) => {
          this.colorService.updateColorToAllFurnitures(
            this.groupService.furnitures,
            color.colorId
          );
          this.dialogRef.close({
            success: true,
            name: 'Farbe',
          });
        });
        break;

      default:
        throw new Error('unknown type:' + type);
    }
  }

  public uploadPic(event: any, type: number) {
    if (type === 1) {
      this.updateGroupImage(event);
    } else {
      this.updateCategoryImage(event);
    }
  }

  private updateGroupImage(event: any) {
    const file = event.target.files.item(0);
    this.newImage = file.name;
    this.uploadService
      .add(file, 'furniture')
      .task.then((data) => {
        this.groupService.getFurnitureImage(file.name).then((image) => {
          this.imagePreviewPath = image;
        });
      })
      .catch((e) => {
        throw new Error(e);
      });
  }

  private updateCategoryImage(event: any) {
    const file = event.target.files.item(0);
    this.newImage = file.name;
    this.uploadService
      .add(file, 'furniture')
      .task.then((data) => {
        this.categoryService.getImage(file.name).then((image) => {
          this.imageCategoryPreviewPath = image;
        });
      })
      .catch((e) => {
        throw new Error(e);
      });
  }

  public compareFn(v1: Color, v2: Color): boolean {
    return compareFn(v1, v2);
  }

  private prepairFormgroup(): void {
    switch (this.typeOfEdit) {
      case 1:
        this.groupForm = this.fg.group({
          partnumber: [this.data.item.partnumber, Validators.required],
          name: [this.data.item.name, Validators.required],
          color: [this.data.item.color, Validators.required],
          measurements: [this.data.item.measurements, Validators.required],
          description: [this.data.item.description, Validators.required],
        });
        this.imagePreviewPath = this.data.item.imageDownloadPath;
        break;

      case 2:
        this.categoryForm = this.fg.group({
          name: [this.data.item.name, Validators.required],
          description: [this.data.item.description, Validators.required],
        });
        this.imageCategoryPreviewPath = this.data.item.imageDownloadPath;
        break;
      case 3:
        this.storageForm = this.fg.group({
          name: [this.data.item.name, Validators.required],
        });
        break;
      case 4:
        this.colorForm = this.fg.group({
          colorName: [this.data.item.colorName, Validators.required],
          colorCode: [this.data.item.colorCode, Validators.required],
        });
        break;

      default:
        throw new Error('unknown type:' + this.typeOfEdit);
    }
  }
}

function compareFn(v1: Color, v2: Color): boolean {
  return v1 && v2 ? v1.colorId === v2.colorId : v1 === v2;
}
