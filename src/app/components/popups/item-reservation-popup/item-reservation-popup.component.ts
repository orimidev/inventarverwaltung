import {
  animate,
  group,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { DatePipe } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MatDialogRef, MatTabGroup, MAT_DIALOG_DATA } from '@angular/material';
import { takeWhile } from 'rxjs/operators';
import { SettingsService } from 'src/app/services/backend/settings.service';
import { isNullOrUndefined } from 'util';
import { FurnitureGroup } from '../../../models/furnitureGroup.model';
import { FurnitureGroupService } from '../../../services/furniture-group.service';

enum SWITCHFORMSTATUS {
  notSwitch,
  notRegistered,
}

@Component({
  selector: 'app-item-reservation-popup',
  templateUrl: './item-reservation-popup.component.html',
  styleUrls: ['./item-reservation-popup.component.scss'],
  animations: [
    trigger('enterAnimation', [
      transition(':enter', [
        style({ height: '0px', overflow: 'hidden' }),
        group([animate('500ms ease-out', style({ height: '!' }))]),
      ]),
      transition(':leave', [
        style({ height: '!', overflow: 'hidden' }),
        group([animate('250ms ease-out', style({ height: '0px' }))]),
      ]),
    ]),
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ItemReservationPopupComponent implements OnInit, OnDestroy {
  @ViewChild('tabGroup', { static: false }) public tabGroup: MatTabGroup;
  public count = 1;
  public toGivecount = 1;
  public switchStatus = SWITCHFORMSTATUS.notSwitch;
  public searchObj: any;
  public dataArray: FurnitureGroup[];
  public selectedNewItem: FurnitureGroup;
  public mesurementsForNewItem = '';
  public deskriptionForNewItem = '';
  public nameForNewItem = '';
  public mailToSend = '';
  public dateToPickup: Date;

  /**
   * unsubscribe
   */
  private alive = true;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<ItemReservationPopupComponent>,
    public itemFurnitureGroupService: FurnitureGroupService,
    private datePipe: DatePipe,
    private settingsService: SettingsService
  ) {
    this.dataArray = this.itemFurnitureGroupService.furnitures;
  }

  public selectionChanged() {
    if (this.selectedNewItem) {
      this.nameForNewItem = this.selectedNewItem.name;
      this.mesurementsForNewItem = this.selectedNewItem.measurements;
      this.deskriptionForNewItem = this.selectedNewItem.description;
    }
    this.updateMailToSend();
  }

  public ngOnDestroy(): void {
    this.alive = false;
  }

  public ngOnInit(): void {}

  public onSave(): void {
    // TODO send mail
    this.dialogRef.close({ success: true, count: this.count });
  }

  public increaseCount(): void {
    if (
      this.count <
      this.itemFurnitureGroupService.getTotalStockCount(this.data.item)
    ) {
      this.count++;
    }
  }

  public decreaseCount(): void {
    if (this.count > 1) {
      this.count--;
    }
  }

  public furnitureToGiveDecreaseCount(): void {
    if (this.toGivecount > 1) {
      this.toGivecount--;
    }
  }

  public furnitureToGiveIncreaseCount(): void {
    this.toGivecount++;
  }

  public haveItemToSend(): void {
    this.switchStatus = SWITCHFORMSTATUS.notRegistered;
  }

  public goToStep(step: number): void {
    this.tabGroup.selectedIndex = step;
  }

  public turnBack(lastIndex: number): void {
    this.switchStatus = lastIndex;
  }

  public onKey(value): void {
    this.dataArray = [];
    if (value !== '' && value.replace(/ /g, '') !== '') {
      this.selectSearch(value);
    }
  }

  public selectSearch(value: string) {
    const filter = value.toLowerCase();
    this.dataArray = [];
    this.itemFurnitureGroupService.furnitures$
      .pipe(takeWhile(() => this.alive))
      .subscribe((d) => {
        d.forEach((g) => {
          const option = g;
          if (
            option.name.toLowerCase().indexOf(filter) >= 0 ||
            option.measurements
              .replace(/ /g, '')
              .split('.')
              .join('')
              .toLowerCase()
              .indexOf(filter) >= 0 ||
            option.description.toLowerCase().indexOf(filter) >= 0
          ) {
            this.dataArray.push(option);
          }
        });
      });
  }

  public onReservationButtonClicked(): void {
    const email = this.settingsService.settings.emailContact;
    window.open(
      'mailto:' +
        email +
        '?subject=' +
        'Reservierung/' +
        this.data.item.name +
        '&body=' +
        this.mailToSend.replace(/\n/g, '%0a')
    );
    this.dialogRef.close({ success: true, count: this.count });
  }

  public updateMailToSend() {
    let giveMsg = '';
    if (this.catchUnknown(this.nameForNewItem) !== '') {
      giveMsg =
        '\n_____________________________ \n' +
        'Abgeben \n' +
        '  Item: ' +
        this.catchUnknown(this.nameForNewItem) +
        '\n  Anzahl: ' +
        this.catchUnknown(this.toGivecount.toString()) +
        '\n  Maß: ' +
        this.catchUnknown(this.mesurementsForNewItem);
    } else {
      giveMsg = '';
    }

    if (!isNullOrUndefined(this.dateToPickup)) {
      this.mailToSend =
        'Wunschtermin : ' +
        this.datePipe.transform(this.dateToPickup, 'dd.MM.yyyy') +
        '\n \n Reservierung \n' +
        '  Item: ' +
        this.catchUnknown(this.data.item.name) +
        '\n  Anzahl: ' +
        this.catchUnknown(this.count.toString()) +
        '\n  Maß: ' +
        this.catchUnknown(this.data.item.measurements) +
        giveMsg +
        '\n_____________________________\n';
    } else {
      this.mailToSend = 'Wunschtermin bitte eintragen!';
    }
  }

  private catchUnknown(data: string): string {
    if (!isNullOrUndefined(data) && data.trim() !== '') {
      return data;
    } else {
      return '';
    }
  }
}
