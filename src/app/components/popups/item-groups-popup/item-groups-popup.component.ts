import { Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import {
  MatDialog,
  MatDialogRef,
  MatSnackBar,
  MAT_DIALOG_DATA,
} from '@angular/material';
import { FurnitureGroupService } from 'src/app/services/furniture-group.service';
import { isNullOrUndefined } from 'util';
import { FurnitureGroup } from '../../../models/furnitureGroup.model';
import { StorageModel } from '../../../models/storage.model';
import { ItemCreateDetailPopupComponent } from '../item-create-detail-popup/item-create-detail-popup.component';

@Component({
  selector: 'app-group-details',
  templateUrl: './item-groups-popup.component.html',
  styleUrls: ['./item-groups-popup.component.scss'],
})
export class ItemGroupsPopupComponent implements OnInit, OnDestroy {
  @ViewChild('allRoomToggle', { static: false }) public allRoomToggle: any;

  public group: FurnitureGroup;

  /**
   * unsubscribe
   */
  private alive = true;

  constructor(
    public dialogRef: MatDialogRef<ItemGroupsPopupComponent>,
    public itemGroupService: FurnitureGroupService,
    private dialog: MatDialog,
    private snackbar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.group = data.item;
  }

  public ngOnInit() {}

  public onItemClick(store: StorageModel) {
    if (!isNullOrUndefined(store)) {
      const dialogRef = this.dialog.open(ItemCreateDetailPopupComponent, {
        width: '98vw',
        data: { item: this.group, store },
      });

      dialogRef.afterClosed().subscribe((result) => {
        if (!!result) {
          if (result.count !== store.stock) {
            this.itemGroupService
              .updateStock(this.group, store.id, result.count)
              .then(() => {
                this.snackbar.open(
                  `Bestand auf ${result.count} geändert!`,
                  'Ok',
                  { duration: 5000 }
                );
              });
          }
        }
      });
    }
  }

  public ngOnDestroy(): void {
    this.alive = false;
  }
}
