import { Component, Inject, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { HeaderService } from 'src/app/services/header.service';
import { AuthService } from '../../../services/backend/auth/auth.service';

@Component({
  selector: 'app-reset-password-popup',
  templateUrl: './reset-password-popup.component.html',
})
export class ResetPasswordPopupComponent implements OnInit {
  public emailForm: FormGroup;
  public showButtonLoading: boolean;

  constructor(
    public header: HeaderService,
    public translate: TranslateService,
    public dialogRef: MatDialogRef<ResetPasswordPopupComponent>,
    public authService: AuthService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fg: FormBuilder
  ) {
    this.emailForm = this.fg.group({
      text: new FormControl('', [Validators.required, Validators.email]),
    });
  }

  public ngOnInit() {}

  public tryReset(): void {
    this.showButtonLoading = true;
    this.authService.resetPassword(this.emailForm.value.text).then(
      (res) => {
        this.showButtonLoading = false;
        this.dialogRef.close({
          success: true,
          email: this.emailForm.value.text,
        });
      },
      (err) => {
        this.showButtonLoading = false;
        this.dialogRef.close({ success: false, error: err });
      }
    );
  }
}
