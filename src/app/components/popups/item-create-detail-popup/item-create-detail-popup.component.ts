import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-item-create-detail-popup',
  templateUrl: './item-create-detail-popup.component.html',
  styleUrls: ['./item-create-detail-popup.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ItemCreateDetailPopupComponent implements OnInit {
  public count = 0;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<ItemCreateDetailPopupComponent>
  ) {}

  public ngOnInit() {
    this.count = this.data.store.stock;
  }

  public onSave(): void {
    this.dialogRef.close({ success: true, count: this.count });
  }

  public increaseCount() {
    this.count++;
  }

  public decreaseCount() {
    if (this.count > 0) {
      this.count--;
    }
  }
}
