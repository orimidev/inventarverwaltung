import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { UsermanagementRoutingModule } from './usermanagement-routing.module';
import { UsermanagementComponent } from './usermanagement.component';

@NgModule({
  declarations: [UsermanagementComponent],
  imports: [UsermanagementRoutingModule, SharedModule],
  providers: [],
})
export class UsermanagementModule {}
