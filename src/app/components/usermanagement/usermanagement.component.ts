import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { TranslateService } from '@ngx-translate/core';
import { UserRoleModel } from 'src/app/models/role.model';
import { CurrentStateService } from 'src/app/services/current-state.service';
import { PopupService } from 'src/app/services/popup.service';
import { UserService } from '../../services/user.service';

const ROLESLIST = [
  { name: 'app.dropdown.roles.0', id: 0 },
  { name: 'app.dropdown.roles.1', id: 1 },
];

@Component({
  selector: 'app-usermanagement',
  templateUrl: './usermanagement.component.html',
  styleUrls: ['./usermanagement.component.scss'],
})
export class UsermanagementComponent implements OnInit {
  public dataSource: any;
  public roleList = ROLESLIST;

  public displayedColumns: string[] = ['fullname', 'email', 'role'];

  constructor(
    public userService: UserService,
    public current: CurrentStateService,
    public translate: TranslateService,
    public popupService: PopupService
  ) {
    this.dataSource = new MatTableDataSource(this.userService.users);
  }

  public ngOnInit(): void {}

  public roleChange(newRole: number, user: UserRoleModel): void {
    this.userService.changeUserRole(user, newRole);
  }

  public applyFilter(filterValue: string): void {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
