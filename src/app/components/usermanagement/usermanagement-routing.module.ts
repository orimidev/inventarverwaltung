import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccessRightsGuard } from 'src/app/guards/accessrights.guard';
import { UsermanagementComponent } from './usermanagement.component';

const routes: Routes = [
  {
    path: 'usermgmt',
    component: UsermanagementComponent,
    canActivate: [AccessRightsGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class UsermanagementRoutingModule {}
