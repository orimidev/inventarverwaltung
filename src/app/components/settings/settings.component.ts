import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { skipWhile, takeWhile } from 'rxjs/operators';
import { SettingsModel } from 'src/app/models/settings.model';
import { PopupService } from 'src/app/services/popup.service';
import { isNullOrUndefined } from 'util';
import { SettingsService } from './../../services/backend/settings.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
})
export class SettingsComponent implements OnInit, OnDestroy {
  public settingsForm: FormGroup;
  private alive = true;
  constructor(
    public popupService: PopupService,
    public settingsService: SettingsService,
    private fg: FormBuilder
  ) {}

  public ngOnInit(): void {
    this.settingsService.settings$
      .pipe(
        takeWhile(() => this.alive),
        skipWhile((x) => isNullOrUndefined(x))
      )
      .subscribe((settings) => {
        this.settingsForm = this.fg.group({
          emailContact: [
            settings.emailContact,
            [Validators.email, Validators.required],
          ],
        });
      });
  }

  public ngOnDestroy(): void {
    this.alive = false;
  }

  public submitSettings(): void {
    this.settingsService
      .update(this.settingsForm.value as SettingsModel)
      .then((res) => {
        this.popupService.openSnackbarWithText(`Einstellungen gespeicht.`);
      });
  }
}
