import { isNullOrUndefined } from 'util';

export class BaseService {
  constructor() {}

  public compare(a, b) {
    // Use toUpperCase() to ignore character casing
    let name = 'name';
    if (isNullOrUndefined(a.name)) {
      isNullOrUndefined(a.room) && !isNullOrUndefined(a.colorName)
        ? (name = 'colorName')
        : !isNullOrUndefined(name) &&
          !isNullOrUndefined(a.room) &&
          !isNullOrUndefined(a.room.name)
        ? (name = 'name')
        : (name = '');
    }
    const firstItem = a[name].toUpperCase();
    const secondItem = b[name].toUpperCase();

    let comparison = 0;
    if (firstItem > secondItem) {
      comparison = 1;
    } else if (firstItem < secondItem) {
      comparison = -1;
    }
    return comparison;
  }
}
