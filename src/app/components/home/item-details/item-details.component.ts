import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { takeWhile } from 'rxjs/operators';
import { StorageModel } from 'src/app/models/storage.model';
import { isNullOrUndefined } from 'util';
import { FurnitureGroupService } from '../../../services/furniture-group.service';
import { PopupService } from '../../../services/popup.service';
import { FurnitureGroup } from './../../../models/furnitureGroup.model';

@Component({
  selector: 'app-item-details',
  templateUrl: './item-details.component.html',
  styleUrls: ['./item-details.component.scss'],
})
export class ItemDetailsComponent implements OnInit, OnDestroy {
  public groups: FurnitureGroup[] = [];
  public allGroups: FurnitureGroup[];

  public categoryId: string;
  public occupited: false;
  /**
   * unsubscribe
   */
  private alive = true;

  constructor(
    public dialogRef: MatDialogRef<ItemDetailsComponent>,
    public popupService: PopupService,
    public itemsGroupService: FurnitureGroupService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.categoryId = data.item.furnitureCategoryId;

    this.getAllGroupsFromService(data.item);

    if (!isNullOrUndefined(this.allGroups)) {
      this.groups = this.allGroups.slice();
    }

    this.occupited = data.occupited;
  }

  public ngOnInit() {}

  public async getAllGroupsFromService(group: FurnitureGroup) {
    this.itemsGroupService.furnitures$
      .pipe(takeWhile(() => this.alive))
      .subscribe((d) => {
        this.allGroups = d.filter((a: FurnitureGroup) => {
          return a.furnitureCategoryId === this.categoryId;
        });
      });
  }

  public onItemClick(event: FurnitureGroup) {
    if (this.itemsGroupService.getTotalStockCount(event) > 0) {
      this.popupService.openItemReservationPopup(event);
    } else {
      this.popupService.openSnackbarWithText(
        `'${event.name}' ist zur Zeit leider nicht verfügbar`
      );
    }
  }

  public ngOnDestroy(): void {
    this.alive = false;
  }

  public applyFilter(filterValue: string): void {
    const val = this.normalizeString(filterValue);
    this.groups = [];
    this.allGroups.forEach((d) => {
      if (
        this.normalizeString(d.name).includes(val) ||
        this.normalizeString(d.measurements).includes(val) ||
        this.normalizeString(d.description).includes(val) ||
        this.normalizeString(d.color.colorName).includes(val) ||
        this.normalizeString(d.partnumber).includes(val)
      ) {
        this.groups.push(d);
      }
    });
  }

  public getTotalStockCount(item: FurnitureGroup) {
    return this.itemsGroupService.getTotalStockCount(item);
  }

  public getStorageNames(storages: StorageModel[]): string {
    let stores = '';
    for (const store of storages) {
      if (store.stock > 0) {
        stores += `${store.name}, `;
      }
    }
    return stores.slice(0, -2);
  }

  public normalizeString(text: string): string {
    return text
      .toLowerCase()
      .replace(/ /g, '')
      .split('.')
      .join('');
  }
}
