import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { CurrentStateService } from 'src/app/services/current-state.service';
import { PopupService } from 'src/app/services/popup.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  constructor(
    private router: Router,
    public userService: UserService,
    public current: CurrentStateService,
    public translate: TranslateService,
    public popupService: PopupService
  ) {}

  public ngOnInit() {}

  public startRegistration(): void {
    this.router.navigate(['/register']);
  }
}
