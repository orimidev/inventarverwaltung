import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { CurrentStateService } from 'src/app/services/current-state.service';
import { HeaderService } from 'src/app/services/header.service';
import { AuthService } from '../../../services/backend/auth/auth.service';
import { PopupService } from '../../../services/popup.service';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;
  public showButtonLoading: boolean;
  public wrongLoginStatus = false;

  constructor(
    public current: CurrentStateService,
    public header: HeaderService,
    public translate: TranslateService,
    public authService: AuthService,
    public popupService: PopupService,
    public userService: UserService,
    private fg: FormBuilder,
    private router: Router
  ) {
    this.loginForm = this.fg.group({
      text: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
    });
  }

  public ngOnInit() {}

  public showRegisPopup(): void {
    this.popupService.openRegistrationPopup();
  }

  public showResetPasswordPopup(): void {
    this.popupService.resetPasswordPopup();
  }

  public tryLogin(): void {
    this.showButtonLoading = true;
    this.authService
      .doLogin(this.loginForm.value.text, this.loginForm.value.password)
      .then(
        (res) => {
          this.showButtonLoading = false;
          this.router.navigate(['home']);
        },
        (err) => {
          this.showButtonLoading = false;
          this.wrongLoginStatus = true;
        }
      );
  }
}
