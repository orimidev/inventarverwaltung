import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { takeWhile } from 'rxjs/operators';
import { PopupService } from 'src/app/services/popup.service';
import { UserService } from 'src/app/services/user.service';
import { FurnitureCategory } from '../../../models/furnitureCategory.model';
import { FurnitureCategoryService } from '../../../services/furniture-category.service';
import { FurnitureGroupService } from '../../../services/furniture-group.service';
import { FurnitureGroup } from './../../../models/furnitureGroup.model';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss'],
})
export class ItemListComponent implements OnInit, OnDestroy {
  public allCatagories: FurnitureCategory[] = [];
  public allFurnitures: FurnitureGroup[] = [];
  private alive = true;

  constructor(
    public router: Router,
    public categoryService: FurnitureCategoryService,
    public groupService: FurnitureGroupService,
    private popupService: PopupService,
    public userService: UserService
  ) {}

  public ngOnInit() {
    this.getCatagoriesFromService();
    this.getFurnitureItems();
  }

  public countGroupByCatId(id: string): number {
    const groups = this.groupService.furnitures$.value.filter((d) => {
      return id === d.furnitureCategoryId;
    });
    return groups.length;
  }

  public getCatagoriesFromService() {
    this.categoryService.categories$
      .pipe(takeWhile(() => this.alive))
      .subscribe((categoriesData) => {
        this.allCatagories = categoriesData;
      });
  }

  public getFurnitureItems() {
    this.groupService.furnitures$
      .pipe(takeWhile(() => this.alive))
      .subscribe((furnitureItems) => {
        this.allFurnitures = furnitureItems;
      });
  }

  public onItemClick(item: FurnitureCategory) {
    if (this.countGroupByCatId(item.furnitureCategoryId) > 0) {
      this.popupService.openItemDetailPopup(item, false);
    }
  }

  public ngOnDestroy(): void {
    this.alive = false;
  }
}
