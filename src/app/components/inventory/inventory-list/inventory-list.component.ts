import {
  AfterViewInit,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { takeWhile } from 'rxjs/operators';
import { FurnitureGroup } from 'src/app/models/furnitureGroup.model';
import { FurnitureGroupService } from 'src/app/services/furniture-group.service';
import { PopupService } from 'src/app/services/popup.service';

@Component({
  selector: 'app-inventory-list',
  templateUrl: './inventory-list.component.html',
})
export class InventoryListComponent
  implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(MatPaginator, { static: false }) public paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  public displayedColumns: string[] = [
    'partnumber',
    'color',
    'name',
    'measurements',
    'count',
    'edit',
    'delete',
  ];

  public dataSource = new MatTableDataSource(this.itemsGroupService.furnitures);

  private alive = true;

  constructor(
    public itemsGroupService: FurnitureGroupService,
    public popupService: PopupService
  ) {}

  public ngOnInit() {
    this.itemsGroupService.furnitures$
      .pipe(takeWhile(() => this.alive))
      .subscribe((data) => {
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.sortingDataAccessor = (item, property) => {
          switch (property) {
            case 'partnumber':
              return item.partnumber;
            case 'color':
              return item.color.colorName;
            case 'name':
              return item.name;
            case 'measurements':
              return item.measurements;
            default:
              return item[property];
          }
        };
        setTimeout(() => (this.dataSource.paginator = this.paginator));
        setTimeout(() => (this.dataSource.sort = this.sort));
      });
  }

  public ngAfterViewInit(): void {
    setTimeout(() => (this.dataSource.paginator = this.paginator));
    setTimeout(() => (this.dataSource.sort = this.sort));
  }

  public ngOnDestroy(): void {
    this.alive = false;
  }

  public applyFilter(filterValue: string): void {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  public deleteGroup(event: FurnitureGroup) {
    const items = this.itemsGroupService.getTotalStockCount(event);
    if (this.itemsGroupService.getTotalStockCount(event) === 0) {
      this.itemsGroupService.delete(event).then((r) => {
        this.popupService.openSnackbarWithText(
          `Möbelgruppe erfolgreich gelöscht!`
        );
      });
    } else {
      this.popupService.openSnackbarWithText(
        `Dieser Möbelgruppe sind noch ${items} Möbelstücke zugewiesen. Du musst diese Möbelstücke zuerst löschen.`,
        'OK',
        20
      );
    }
  }
}
