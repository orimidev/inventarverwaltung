import {
  AfterViewInit,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { takeWhile } from 'rxjs/operators';
import { FurnitureCategory } from 'src/app/models/furnitureCategory.model';
import { FurnitureCategoryService } from 'src/app/services/furniture-category.service';
import { FurnitureGroupService } from 'src/app/services/furniture-group.service';
import { PopupService } from 'src/app/services/popup.service';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
})
export class CategoryListComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(MatPaginator, { static: false }) public paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  public displayedColumnsCatTable: string[] = ['catName', 'edit', 'delete'];
  public dataSourceCat = new MatTableDataSource(
    this.itemsCategoryService.categories
  );

  private alive = true;

  constructor(
    private itemsCategoryService: FurnitureCategoryService,
    private itemsGroupService: FurnitureGroupService,
    public popupService: PopupService
  ) {}

  public ngOnInit() {
    this.itemsCategoryService.categories$
      .pipe(takeWhile(() => this.alive))
      .subscribe((data) => {
        this.dataSourceCat = new MatTableDataSource(data);
        setTimeout(() => (this.dataSourceCat.paginator = this.paginator));
        setTimeout(() => (this.dataSourceCat.sort = this.sort));
      });
  }

  public ngAfterViewInit(): void {
    setTimeout(() => (this.dataSourceCat.paginator = this.paginator));
    setTimeout(() => (this.dataSourceCat.sort = this.sort));
  }

  public ngOnDestroy(): void {
    this.alive = false;
  }

  public applyFilter(filterValue: string): void {
    this.dataSourceCat.filter = filterValue.trim().toLowerCase();
  }

  public deleteCat(event: FurnitureCategory): void {
    const groupsFromCat = this.itemsGroupService.furnitures.filter((a) => {
      return a.furnitureCategoryId === event.furnitureCategoryId;
    });
    if (groupsFromCat.length === 0) {
      this.itemsCategoryService.delete(event).then((r) => {
        this.popupService.openSnackbarWithText(
          `Kategorie erfolgreich gelöscht!`
        );
      });
    } else {
      this.popupService.openSnackbarWithText(
        `Dieser Kategorie sind noch ${groupsFromCat.length} Möbelstücke zugewiesen. Du musst sie erst löschen.`,
        'OK',
        20
      );
    }
  }
}
