import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ColorService } from '../../../services/backend/color.service';
import { Color } from '../../../models/color.model';
import { PopupService } from '../../../services/popup.service';

@Component({
  selector: 'app-color-create',
  templateUrl: './color-create.component.html',
})
export class ColorCreateComponent implements OnInit {
  public firstForm: FormGroup;

  constructor(
    private fg: FormBuilder,
    private popupService: PopupService,
    private colorService: ColorService
  ) {}

  public ngOnInit() {
    this.firstForm = this.fg.group({
      colorName: ['', Validators.required],
      colorCode: ['', Validators.required],
    });
  }

  public submitColor(): void {
    this.colorService.add(this.firstForm.value as Color).then((res) => {
      this.colorService.addIdToColor(res.id);
      this.popupService.openSnackbarWithText(
        'Farbe "' + this.firstForm.value.colorName + '" angelegt!'
      );
      this.firstForm.reset();
    });
  }
}
