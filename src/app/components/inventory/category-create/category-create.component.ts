import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FurnitureCategory } from 'src/app/models/furnitureCategory.model';
import { FurnitureCategoryService } from 'src/app/services/furniture-category.service';
import { ImageUploadService } from 'src/app/services/image-upload.service';
import { PopupService } from 'src/app/services/popup.service';

@Component({
  selector: 'app-category-create',
  templateUrl: './category-create.component.html',
})
export class CategoryCreateComponent implements OnInit, OnDestroy {
  public firstFormCategory: FormGroup;
  public imageCategoryPreviewPath = '';

  private alive = true;

  constructor(
    public uploadService: ImageUploadService,
    private fg: FormBuilder,
    private itemsCategoryService: FurnitureCategoryService,
    private popupService: PopupService
  ) {}

  public ngOnInit() {
    this.firstFormCategory = this.fg.group({
      imgPath: ['', Validators.required],
      name: ['', Validators.required],
      description: ['', Validators.required],
    });
  }

  public ngOnDestroy(): void {
    this.alive = false;
  }

  public submitCategory(): void {
    this.itemsCategoryService
      .add(this.firstFormCategory.value as FurnitureCategory)
      .then((res) => {
        this.itemsCategoryService.addIdToFurnitureCategory(res.id);
        this.popupService.openSnackbarWithText(`Kategorie angelegt!`);
        this.imageCategoryPreviewPath = '';
        this.firstFormCategory.reset();
      });
  }

  public uploadImage(event: any) {
    const file = event.target.files.item(0);
    this.firstFormCategory.patchValue({ imgPath: file.name });
    this.uploadService
      .add(file, 'furniture')
      .task.then((data) => {
        this.itemsCategoryService.getImage(file.name).then((image) => {
          this.imageCategoryPreviewPath = image;
        });
      })
      .catch((e) => {
        throw new Error(e);
      });
  }
}
