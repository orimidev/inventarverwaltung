import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccessRightsGuard } from 'src/app/guards/accessrights.guard';
import { InventoryComponent } from './inventory.component';

const routes: Routes = [
  {
    path: 'inventory',
    component: InventoryComponent,
    canActivate: [AccessRightsGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class InventoryRoutingModule {}
