import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { takeWhile } from 'rxjs/operators';
import { StorageModel } from 'src/app/models/storage.model';
import { FurnitureGroup } from '../../../models/furnitureGroup.model';
import { FurnitureGroupService } from '../../../services/furniture-group.service';
import { StorageRoomService } from './../../../services/storage.service';

@Component({
  selector: 'app-storage-create',
  templateUrl: './storage-create.component.html',
})
export class StorageCreateComponent implements OnInit, OnDestroy {
  public firstFormStorage: FormGroup;
  public allFurnitures: FurnitureGroup[];
  private alive = true;

  constructor(
    private fg: FormBuilder,
    public storageRoomService: StorageRoomService,
    public furnitureGroupService: FurnitureGroupService
  ) {}

  public ngOnInit() {
    this.firstFormStorage = this.fg.group({
      name: ['', Validators.required],
    });

    this.furnitureGroupService.furnitures$
      .pipe(takeWhile(() => this.alive))
      .subscribe((data) => {
        this.allFurnitures = data;
      });
  }

  public ngOnDestroy(): void {
    this.alive = false;
  }

  public submitRoom(): void {
    this.storageRoomService
      .add(this.firstFormStorage.value as StorageModel)
      .then((res) => {
        this.storageRoomService.addIdToStore(res.id).then(() => {
          this.storageRoomService.addOrDeleteNewStorageToAllFurnitures(
            this.allFurnitures,
            res.id,
            true
          );
        });
        this.firstFormStorage.reset();
      });
  }
}
