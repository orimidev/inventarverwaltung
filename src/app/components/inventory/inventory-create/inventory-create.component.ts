import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { skipWhile, takeWhile } from 'rxjs/operators';
import { isNullOrUndefined } from 'util';

import { Color } from 'src/app/models/color.model';
import { FurnitureCategory } from 'src/app/models/furnitureCategory.model';
import { FurnitureGroup } from 'src/app/models/furnitureGroup.model';
import { ColorService } from 'src/app/services/backend/color.service';
import { FurnitureCategoryService } from 'src/app/services/furniture-category.service';
import { FurnitureGroupService } from 'src/app/services/furniture-group.service';
import { PopupService } from 'src/app/services/popup.service';
import { InventoryViewService } from '../services/inventory-view.service';
import { ImageUploadService } from './../../../services/image-upload.service';
import { StorageRoomService } from '../../../services/storage.service';

@Component({
  selector: 'app-inventory-create',
  templateUrl: './inventory-create.component.html',
})
export class InventoryCreateComponent implements OnInit, OnDestroy {
  public firstFormGroup: FormGroup;
  public secondFormGroup: FormGroup;
  public selectedColor: Color;
  public imagePreviewPath = '';
  public categorieSearchResults: FurnitureCategory[] = [];
  public selectedCategory = '';

  @Output() changeToInventoryList = new EventEmitter();

  private alive = true;

  constructor(
    private fg: FormBuilder,
    public colorService: ColorService,
    public itemsGroupService: FurnitureGroupService,
    public itemsCategoryService: FurnitureCategoryService,
    public storageRoomService: StorageRoomService,
    public uploadService: ImageUploadService,
    private popupService: PopupService,
    private inventoryVS: InventoryViewService
  ) {}

  public ngOnInit() {
    this.firstFormGroup = this.fg.group({
      imgPath: ['', Validators.required],
    });
    this.secondFormGroup = this.fg.group({
      partnumber: ['', Validators.required],
      name: ['', Validators.required],
      measurements: ['', Validators.required],
      color: [this.selectedColor, Validators.required],
      description: ['', Validators.required],
      furnitureCategoryId: ['', Validators.required],
    });

    this.itemsCategoryService.categories$
      .pipe(takeWhile(() => this.alive))
      .subscribe((categoriesData) => {
        this.categorieSearchResults = categoriesData;
      });
  }

  public ngOnDestroy(): void {
    this.alive = false;
  }

  public submit(): void {
    const form = {
      ...this.firstFormGroup.value,
      ...this.secondFormGroup.value,
    } as FurnitureGroup;
    form.storages = [];
    this.storageRoomService.storages$.value.forEach((d) => {
      form.storages.push({ ...d, stock: 0 });
    });

    let hasShownPopup = false;
    this.itemsGroupService.add(form).then((res) => {
      this.popupService.openSnackbarWithText(`Möbelstück angelegt!`);
      this.imagePreviewPath = '';
      this.secondFormGroup.reset();
      this.firstFormGroup.reset();
      this.itemsGroupService.addIdToFurnitureGroup(res.id).then(() => {
        let item: FurnitureGroup;
        this.itemsGroupService.furnitures.forEach((d) => {
          if (res.id === d.furnitureGroupId) {
            item = d;
          }
        });
        this.changeToInventoryList.emit(null);
        if (!isNullOrUndefined(item)) {
          this.popupService.openItemsDetailPopup(item);
        }

        this.itemsGroupService.furnitures$
          .pipe(
            takeWhile(() => hasShownPopup === false),
            skipWhile(
              (furnitures) =>
                furnitures.findIndex(
                  (index) => index.furnitureGroupId === res.id
                ) === -1
            )
          )
          .subscribe((furnitures) => {
            hasShownPopup = true;
            this.popupService.openItemsDetailPopup(
              this.itemsGroupService.getGroup(res.id)
            );
          });
      });
    });
  }

  public uploadImage(event: any) {
    const file = event.target.files.item(0);
    this.firstFormGroup.patchValue({ imgPath: file.name });
    this.uploadService
      .add(file, 'furniture')
      .task.then((data) => {
        this.itemsGroupService.getFurnitureImage(file.name).then((image) => {
          this.imagePreviewPath = image;
        });
      })
      .catch((e) => {
        throw new Error(e);
      });
  }

  public categoryOnCreate(): void {
    this.inventoryVS.tabIndex = 1;
  }

  public onKey(value): void {
    this.categorieSearchResults = [];
    if (value !== '' && value.replace(/ /g, '') !== '') {
      this.selectSearch(value);
    } else {
      this.categorieSearchResults = this.itemsCategoryService.categories;
    }
  }

  private selectSearch(value: string) {
    const filter = value.toLowerCase();
    this.categorieSearchResults = [];

    this.itemsCategoryService.categories.forEach((d) => {
      if (
        d.name.toLowerCase().indexOf(filter) >= 0 ||
        d.description
          .replace(/ /g, '')
          .split('.')
          .join('')
          .toLowerCase()
          .indexOf(filter) >= 0
      ) {
        this.categorieSearchResults.push(d);
      }
    });
  }
}
