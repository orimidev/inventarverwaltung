import { Component, OnDestroy, OnInit } from '@angular/core';
import { InventoryViewService } from './services/inventory-view.service';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
})
export class InventoryComponent implements OnInit, OnDestroy {
  public showRoomTable = true;
  public showColorTable = true;
  public showCatTable = true;
  public showGroupTable = true;

  private alive = true;

  constructor(public inventoryVS: InventoryViewService) {}

  public ngOnInit() {}

  public ngOnDestroy(): void {
    this.alive = false;
  }

  /**
   *
   * @param index: number => 1: "groupmask"; 2: "categorymask"; 3: "roommask"
   * @param type: number => 1 : "addmode"; 2 : "listmode"
   */
  public switchStatusOfMask(index: number, type: 1 | 2): void {
    const listMode = type === 2;

    switch (index) {
      case 1:
        this.showGroupTable = listMode;
        break;
      case 2:
        this.showCatTable = listMode;
        break;
      case 3:
        this.showRoomTable = listMode;
        break;
      case 4:
        this.showColorTable = listMode;
        break;
      default:
        throw new Error('unknown Index: ' + index);
    }
  }
}
