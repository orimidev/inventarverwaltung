import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { CategoryCreateComponent } from './category-create/category-create.component';
import { CategoryListComponent } from './category-list/category-list.component';
import { InventoryCreateComponent } from './inventory-create/inventory-create.component';
import { InventoryListComponent } from './inventory-list/inventory-list.component';
import { InventoryRoutingModule } from './inventory-routing.module';
import { InventoryComponent } from './inventory.component';
import { StorageCreateComponent } from './storage-create/storage-create.component';
import { StorageListComponent } from './storage-list/storage-list.component';
import { ColorCreateComponent } from './color-create/color-create.component';
import { ColorListComponent } from './color-list/color-list.component';

@NgModule({
  declarations: [
    InventoryComponent,
    ColorCreateComponent,
    ColorListComponent,
    InventoryListComponent,
    InventoryCreateComponent,
    CategoryCreateComponent,
    CategoryListComponent,
    StorageCreateComponent,
    StorageListComponent,
  ],
  imports: [InventoryRoutingModule, SharedModule],
  providers: [],
})
export class InventoryModule {}
