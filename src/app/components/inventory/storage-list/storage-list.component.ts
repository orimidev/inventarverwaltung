import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { takeWhile } from 'rxjs/operators';
import { StorageModel } from 'src/app/models/storage.model';
import { PopupService } from 'src/app/services/popup.service';
import { StorageRoomService } from 'src/app/services/storage.service';
import { FurnitureGroup } from '../../../models/furnitureGroup.model';
import { FurnitureGroupService } from '../../../services/furniture-group.service';

@Component({
  selector: 'app-storage-list',
  templateUrl: './storage-list.component.html',
})
export class StorageListComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator, { static: false }) public paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  public displayedColumnsRoomTable: string[] = ['name', 'edit', 'delete'];
  public dataSourceRooms = new MatTableDataSource(
    this.storageRoomService.storages
  );
  public allFurnitures: FurnitureGroup[];

  private alive = true;

  constructor(
    private storageRoomService: StorageRoomService,
    public popupService: PopupService,
    public furnitureGroupService: FurnitureGroupService
  ) {}

  public ngOnInit() {
    this.alive = true;
    this.storageRoomService.storages$
      .pipe(takeWhile(() => this.alive))
      .subscribe((data) => {
        this.dataSourceRooms = new MatTableDataSource(data);
        setTimeout(() => (this.dataSourceRooms.paginator = this.paginator));
        setTimeout(() => (this.dataSourceRooms.sort = this.sort));
      });

    this.furnitureGroupService.furnitures$
      .pipe(takeWhile(() => this.alive))
      .subscribe((data) => {
        this.allFurnitures = data;
      });
  }

  public ngOnDestroy(): void {
    this.alive = false;
  }

  public deleteRoom(event: StorageModel): void {
    const items = this.furnitureGroupService.getTotalStockCountForStorage(
      event.id
    );
    if (items === 0) {
      this.storageRoomService.delete(event, this.allFurnitures).then((r) => {
        this.popupService.openSnackbarWithText(
          `Lagerort erfolgreich gelöscht!`
        );
      });
    } else {
      this.popupService.openSnackbarWithText(
        `Diesem Lagerort sind noch ${items} Möbelstücke zugewiesen. Du musst sie erst löschen.`,
        'OK',
        20
      );
    }
  }

  public applyFilter(filterValue: string): void {
    this.dataSourceRooms.filter = filterValue.trim().toLowerCase();
  }
}
