import {
  AfterViewInit,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { takeWhile } from 'rxjs/operators';
import { PopupService } from 'src/app/services/popup.service';
import { Color } from '../../../models/color.model';
import { ColorService } from '../../../services/backend/color.service';
import { FurnitureGroupService } from '../../../services/furniture-group.service';

@Component({
  selector: 'app-color-list',
  templateUrl: './color-list.component.html',
})
export class ColorListComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild(MatPaginator, { static: false }) public paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  public displayedColumnsColorsTable: string[] = [
    'colorName',
    'colorCode',
    'edit',
    'delete',
  ];

  public dataSourceColors = new MatTableDataSource(this.colorService.colors);

  private alive = true;

  constructor(
    public popupService: PopupService,
    private colorService: ColorService,
    private itemsGroupService: FurnitureGroupService
  ) {}

  public ngOnInit() {
    this.colorService.colors$
      .pipe(takeWhile(() => this.alive))
      .subscribe((data) => {
        this.dataSourceColors = new MatTableDataSource(data);
        setTimeout(() => (this.dataSourceColors.paginator = this.paginator));
        setTimeout(() => (this.dataSourceColors.sort = this.sort));
      });
  }

  public ngAfterViewInit(): void {
    setTimeout(() => (this.dataSourceColors.paginator = this.paginator));
    setTimeout(() => (this.dataSourceColors.sort = this.sort));
  }

  public ngOnDestroy(): void {
    this.alive = false;
  }

  public deleteColor(event: Color): void {
    const groupsWithColorToDelete = this.itemsGroupService.furnitures.filter(
      (a) => {
        return a.color.colorId === event.colorId;
      }
    );
    if (groupsWithColorToDelete.length === 0) {
      this.colorService.delete(event).then((r) => {
        this.popupService.openSnackbarWithText(
          'Farbe "' + event.colorName + '" erfolgreich gelöscht!'
        );
      });
    } else {
      this.popupService.openSnackbarWithText(
        groupsWithColorToDelete.length === 1
          ? `1 Möbelstück ist vorhanden, welches die Farbe " ${event.colorName} " hat. Löschen Sie zu erst dieses Möbelstück`
          : `${groupsWithColorToDelete.length} Möbelstücke sind vorhanden, welche die Farbe " ${event.colorName} " haben. Löschen Sie zu erst diese Möbelstücke`,
        'OK',
        20
      );
    }
  }

  public applyFilter(filterValue: string): void {
    this.dataSourceColors.filter = filterValue.trim().toLowerCase();
  }
}
