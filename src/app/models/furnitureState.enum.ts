export enum FurnitureStateEnum {
  Free,
  Reserved,
  Occupied,
}
