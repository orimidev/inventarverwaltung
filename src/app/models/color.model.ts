export class Color {
  public colorId: string;
  public colorName: string;
  public colorCode: string;
}
