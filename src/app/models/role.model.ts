import { UserRoleEnum } from './roles.enum';

export class UserRoleModel {
  public userId: string;
  public userEmail: string;
  public userName: string;
  public role: UserRoleEnum;

  constructor(
    userId: string,
    userEmail: string,
    userName: string,
    role: UserRoleEnum
  ) {
    this.userId = userId;
    this.userEmail = userEmail;
    this.userName = userName;
    this.role = role;
  }
}
