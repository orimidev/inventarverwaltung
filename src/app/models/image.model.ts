export class ImageModel {
  public key?: string;
  public originalPath?: string;
  public path?: string;

  constructor() {
    this.key = '';
    this.originalPath = '';
    this.path = '';
  }
}
