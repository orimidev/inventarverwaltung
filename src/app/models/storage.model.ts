export class StorageModel {
  public id: string;
  public name: string;
  public stock?: number;
}
