import { Color } from './color.model';
import { StorageModel } from './storage.model';

export class FurnitureGroup {
  public furnitureGroupId: string;
  public name: string;
  public measurements: string;
  public description: string;
  public partnumber: string;
  public imgPath: string;
  public color: Color;
  public furnitureCategoryId?: string;
  public storages: StorageModel[];

  // only local
  public imageDownloadPath?: string;
}
