export class FurnitureCategory {
  public furnitureCategoryId: string;
  public name: string;
  public description: string;
  public imgPath: string;

  // only loacal
  public imageDownloadPath?: string;
}
