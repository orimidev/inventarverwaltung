import { UserRoleEnum } from './roles.enum';

export class User {
  public userid: string;
  public email: string;
  public role: UserRoleEnum;
}
