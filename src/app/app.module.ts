import { DatePipe } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ToastrModule } from 'ngx-toastr';
import { environment } from '../environments/environment.prod';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeModule } from './components/home/home.module';
import { InventoryModule } from './components/inventory/inventory.module';
import { EditPopupComponent } from './components/popups/edit-popup/edit-popup.component';
import { ErrorPopupComponent } from './components/popups/error-popup/error-popup';
import { ItemCreateDetailPopupComponent } from './components/popups/item-create-detail-popup/item-create-detail-popup.component';
import { ItemGroupsPopupComponent } from './components/popups/item-groups-popup/item-groups-popup.component';
import { ItemReservationPopupComponent } from './components/popups/item-reservation-popup/item-reservation-popup.component';
import { RegisPopupComponent } from './components/popups/regis/regis-popup.component';
import { SuperSearchPopupComponent } from './components/popups/super-search-popup/super-search-popup.component';
import { SettingsModule } from './components/settings/settings.module';
import { MaterialModule } from './components/shared/material-module';
import { SharedModule } from './components/shared/shared.module';
import { UsermanagementModule } from './components/usermanagement/usermanagement.module';
import { ResetPasswordPopupComponent } from './components/popups/reset-password-popup/reset-password-popup.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    SuperSearchPopupComponent,
    ErrorPopupComponent,
    EditPopupComponent,
    RegisPopupComponent,
    ItemGroupsPopupComponent,
    ResetPasswordPopupComponent,
    ItemCreateDetailPopupComponent,
    ItemReservationPopupComponent,
  ],
  entryComponents: [
    ErrorPopupComponent,
    RegisPopupComponent,
    SuperSearchPopupComponent,
    EditPopupComponent,
    ResetPasswordPopupComponent,
    ItemCreateDetailPopupComponent,
    ItemGroupsPopupComponent,
    ItemReservationPopupComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SharedModule,
    HomeModule,
    InventoryModule,
    UsermanagementModule,
    SettingsModule,
    HttpClientModule,
    MaterialModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireAuthModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    ToastrModule.forRoot(),
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent],
  exports: [ItemCreateDetailPopupComponent, ItemReservationPopupComponent],
})
export class AppModule {}
