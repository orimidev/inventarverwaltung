import { Injectable } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { ItemDetailsComponent } from '../components/home/item-details/item-details.component';
import { EditPopupComponent } from '../components/popups/edit-popup/edit-popup.component';
import { ErrorPopupComponent } from '../components/popups/error-popup/error-popup';
import { ItemGroupsPopupComponent } from '../components/popups/item-groups-popup/item-groups-popup.component';
import { ItemReservationPopupComponent } from '../components/popups/item-reservation-popup/item-reservation-popup.component';
import { RegisPopupComponent } from '../components/popups/regis/regis-popup.component';
import { ResetPasswordPopupComponent } from '../components/popups/reset-password-popup/reset-password-popup.component';
import { SuperSearchPopupComponent } from '../components/popups/super-search-popup/super-search-popup.component';
import { Color } from '../models/color.model';
import { FurnitureCategory } from '../models/furnitureCategory.model';
import { FurnitureGroup } from '../models/furnitureGroup.model';
import { StorageModel } from '../models/storage.model';
import { TranslationsService } from './translations.service';

@Injectable({
  providedIn: 'root',
})
export class PopupService {
  constructor(
    private dialog: MatDialog,
    private snackbar: MatSnackBar,
    private localTranslate: TranslationsService
  ) {}

  public openErrorpopup(title: string, text: string, buttonText: string): void {
    this.dialog.open(ErrorPopupComponent, {
      height: '30vh',
      width: '40vw',
      data: { title, text, buttonText },
    });
  }

  public openSuperSearchPopup(): void {
    this.dialog.open(SuperSearchPopupComponent, {
      width: '100vw',
      data: {},
    });
  }

  public openSnackbarWithText(
    text: string,
    actionText?: string,
    duration?: number
  ): void {
    this.snackbar.open(text, actionText, {
      duration: duration ? duration * 1000 : 5000,
      verticalPosition: 'bottom',
    });
  }

  public openRegistrationPopup(): void {
    const dialogRef = this.dialog.open(RegisPopupComponent, {
      width: '100vw',
      data: {},
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (!result) {
        return;
      }
      if (result.success === false) {
        this.openSnackbarWithText(
          result.error.message,
          this.localTranslate.confirmButton
        );
      }
    });
  }

  public resetPasswordPopup(): void {
    const dialogRef = this.dialog.open(ResetPasswordPopupComponent, {
      width: '100vw',
      data: {},
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (!result) {
        return;
      }
      if (result.success === false) {
        this.openSnackbarWithText(
          result.error.message,
          this.localTranslate.confirmButton
        );
      } else if (result.success === true) {
        this.openSnackbarWithText(
          `Wir haben dir eine Email mit Anweisungen zum zurücksetzen deines Passworts an ${result.email} gesendet.`,
          'Ok',
          30
        );
      }
    });
  }

  public openItemsDetailPopup(item: FurnitureGroup): void {
    const dialogRef = this.dialog.open(ItemGroupsPopupComponent, {
      width: '100vw',
      data: { item },
    });
  }

  /**
   * Edit Popup
   * @param item => Item to Edit
   * @param type => Type of Edit: number => 1: Group; 2: Category; 3: Room
   */
  public openEditGroupCatRoomPopup(
    item: FurnitureGroup | FurnitureCategory | StorageModel | Color,
    type: number
  ): void {
    const dialogRef = this.dialog.open(EditPopupComponent, {
      width: '100vw',
      data: { item, type },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (!!result) {
        this.openSnackbarWithText(`${result.name} wurde geändert!`);
      }
    });
  }

  public openItemReservationPopup(item: FurnitureGroup): void {
    const dialogRef = this.dialog.open(ItemReservationPopupComponent, {
      width: '78vw',
      data: { item },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (!!result) {
        this.openSnackbarWithText(`${result.count} ${item.name} reserviert!`);
      }
    });
  }

  public openItemDetailPopup(
    item: FurnitureCategory,
    occupited: boolean
  ): void {
    const dialogRef = this.dialog.open(ItemDetailsComponent, {
      width: '100vw',
      data: { item, occupited },
    });
  }
}
