import { Injectable } from '@angular/core';
import {
  AngularFireStorage,
  AngularFireUploadTask,
} from '@angular/fire/storage';
import { Observable } from 'rxjs/internal/Observable';
import { isNullOrUndefined } from 'util';

@Injectable({
  providedIn: 'root',
})
export class ImageUploadService {
  // Download URL
  public path: string;
  // Progress monitoring
  public percentage: Observable<number>;
  public snapshot: Observable<any>;
  // Main task
  public task: AngularFireUploadTask;

  constructor(private storage: AngularFireStorage) {}

  public add(file: any, path: string) {
    // The File object
    if (!isNullOrUndefined(file)) {
      // Client-side validation example
      if (file.type.split('/')[0] !== 'image') {
        throw new Error('unsupported file type : ' + file.type);
      }

      // The storage path
      this.path = `${path}/${file.name}`;

      const img = new Image();
      img.src = window.URL.createObjectURL(file);

      // Totally optional metadata
      const customMetadata = {
        app: 'Lagerfuchs',
        // user: this.current.user.accountName
      };

      // The main task

      this.task = this.storage.upload(this.path, file, { customMetadata });
      this.percentage = this.task.percentageChanges();
      this.snapshot = this.task.snapshotChanges();
      return this.task;
    }
  }
}
