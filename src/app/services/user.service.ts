import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { isNullOrUndefined } from 'util';
import { BaseService } from '../components/shared/base.service';
import { UserRoleModel } from '../models/role.model';
import { UserRoleEnum } from '../models/roles.enum';
import { AuthService } from './backend/auth/auth.service';

@Injectable({
  providedIn: 'root',
})
export class UserService extends BaseService {
  public currentUser: UserRoleModel;
  public users: UserRoleModel[];
  public initialzed$ = new BehaviorSubject(false);
  public possibleRoles: UserRoleEnum[] = [
    UserRoleEnum.Employee,
    UserRoleEnum.StorageWorker,
  ];

  private readonly CK_USER = 'user';

  constructor(private afs: AngularFirestore, private authService: AuthService) {
    super();
    this.subscribeToUsers();
  }

  public isUserLoggedIn(): boolean {
    return !!this.currentUser;
  }

  public isStorageWorker(): boolean {
    return (
      this.isUserLoggedIn() &&
      this.currentUser.role === UserRoleEnum.StorageWorker
    );
  }

  public addNewUser(data: UserRoleModel) {
    return this.afs
      .collection(this.CK_USER)
      .doc(data.userEmail)
      .set(Object.assign({}, data));
  }

  public changeUserRole(
    user: UserRoleModel,
    newRole: UserRoleEnum
  ): Promise<void> {
    return this.afs
      .doc(`${this.CK_USER}/${user.userEmail}`)
      .update({ role: newRole });
  }

  private subscribeToUsers() {
    return this.afs
      .collection<UserRoleModel>(this.CK_USER)
      .snapshotChanges()
      .subscribe((response) => {
        const users = response.map((e) => {
          return {
            ...e.payload.doc.data(),
          } as UserRoleModel;
        });

        this.users = users;
        this.subscribeToCurrentUser();
      });
  }

  private subscribeToCurrentUser() {
    this.authService.watchUser().subscribe((u) => {
      if (!isNullOrUndefined(u)) {
        let actualUser;
        this.users.forEach((d) => {
          if (d.userId === u.uid) {
            actualUser = new UserRoleModel(
              d.userId,
              d.userEmail,
              d.userName,
              d.role
            );
          }
        });
        this.currentUser = actualUser;
        this.initialzed$.next(true);
      } else {
        this.initialzed$.next(true);
      }
    });
  }
}
