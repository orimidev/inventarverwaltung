import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

import { BaseService } from '../../components/shared/base.service';
import { Color } from '../../models/color.model';
import { FurnitureGroup } from '../../models/furnitureGroup.model';

@Injectable({
  providedIn: 'root',
})
export class ColorService extends BaseService {
  public colors: Color[];
  public colors$ = new BehaviorSubject<Color[]>([]);
  private readonly CK_COLOR = 'color';
  private readonly CK_FURNITEGROUP = 'furniture';

  constructor(private afs: AngularFirestore) {
    super();
    this.subscribeToColorFromFirestore();
  }

  private subscribeToColorFromFirestore() {
    this.afs
      .collection<Color>(this.CK_COLOR)
      .snapshotChanges()
      .subscribe((response) => {
        const colorsFromResponse = response.map((e) => {
          return {
            ...e.payload.doc.data(),
          } as Color;
        });
        this.colors = colorsFromResponse.sort(this.compare);
        this.colors$.next(this.colors);
      });
  }

  public async add(data: Color): Promise<DocumentReference> {
    return this.afs.collection(this.CK_COLOR).add(data);
  }

  public addIdToColor(docId: string): Promise<void> {
    return this.afs.doc(this.CK_COLOR + '/' + docId).update({ colorId: docId });
  }

  public update(color: Color): Promise<void> {
    return this.afs
      .doc(this.CK_COLOR + '/' + color.colorId)
      .update({ ...color });
  }

  public updateColorToAllFurnitures(
    allFurnitureGroups: FurnitureGroup[],
    newColorId: string
  ) {
    let newColor: Color;
    this.colors.forEach((d) => {
      if (d.colorId === newColorId) {
        newColor = d;
      }
    });
    allFurnitureGroups.forEach((d) => {
      if (d.color.colorId === newColorId) {
        this.updateColorFromFurnitureGroup(d, newColor);
      }
    });
  }

  private updateColorFromFurnitureGroup(data: FurnitureGroup, newColor: Color) {
    return this.afs
      .doc(`${this.CK_FURNITEGROUP}/${data.furnitureGroupId}`)
      .update({ color: newColor });
  }

  public delete(data: Color): Promise<void> {
    return this.afs
      .collection(this.CK_COLOR)
      .doc(data.colorId)
      .delete();
  }
}
