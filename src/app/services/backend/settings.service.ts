import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { SettingsModel } from 'src/app/models/settings.model';

@Injectable({
  providedIn: 'root',
})
export class SettingsService {
  public settings: SettingsModel;
  public settings$ = new BehaviorSubject<SettingsModel>(undefined);
  private readonly CK_SETTINGS = 'settings';
  private readonly DOC_ID = 'main';

  constructor(private afs: AngularFirestore) {
    this.getSettings();
  }

  public update(settings: SettingsModel): Promise<void> {
    return this.afs
      .doc(`${this.CK_SETTINGS}/${this.DOC_ID}/`)
      .update({ ...settings });
  }

  private getSettings() {
    this.afs
      .collection<SettingsModel>(this.CK_SETTINGS)
      .doc(this.DOC_ID)
      .snapshotChanges()
      .subscribe((response) => {
        this.settings = response.payload.data() as SettingsModel;
        this.settings$.next(this.settings);
      });
  }
}
