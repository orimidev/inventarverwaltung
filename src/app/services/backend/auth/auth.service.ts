import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private afAuth: AngularFireAuth) {
    //// Get auth data, then get firestore user document || null
    this.watchUser();
  }

  public watchUser(): Observable<any> {
    return this.afAuth.authState;
  }

  // public getAllUsers(): Promise<any> {
  //   // return this.afAuth.auth.
  // }

  public doLogin(email, password): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.auth.signInWithEmailAndPassword(email, password).then(
        (res) => {
          resolve(res);
        },
        (err) => reject(err)
      );
    });
  }

  public signOut(): void {
    this.afAuth.auth.signOut().then(() => {});
  }

  public signUp(email, password): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.auth.createUserWithEmailAndPassword(email, password).then(
        (res) => {
          resolve(res);
        },
        (err) => reject(err)
      );
    });
  }

  public resetPassword(email): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.auth.sendPasswordResetEmail(email).then(
        (res) => {
          resolve(res);
        },
        (err) => reject(err)
      );
    });
  }
}
