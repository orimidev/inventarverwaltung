import { DatePipe, registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';
import localeEn from '@angular/common/locales/en';
import { Injectable } from '@angular/core';
import { DateAdapter } from '@angular/material/core';
import { TranslateService } from '@ngx-translate/core';
import { CurrentStateService } from './current-state.service';

registerLocaleData(localeDe);
registerLocaleData(localeEn);

@Injectable({
  providedIn: 'root',
})
export class TranslationsService {
  public confirmButton = '';
  public datePipe: DatePipe;
  private supportedLangs = ['de-DE', 'en-GB'];

  constructor(
    private translate: TranslateService,
    private current: CurrentStateService,
    private adapter: DateAdapter<any>
  ) {
    this.translate.addLangs(this.supportedLangs);
    this.translate.setDefaultLang('de-DE');

    let browserLang = navigator.language;
    if (!!browserLang) {
      if (browserLang.length === 2) {
        browserLang = this.getFullIsoCode(browserLang);
      }
      // this.translate.use(browserLang);
      this.translate.use('de-DE');
    }

    this.onLangChange();
    this.translate.onLangChange.subscribe(() => {
      this.onLangChange();
    });
  }

  public updateLanguage(lang: string): void {
    this.translate.use(lang);
    /*if (this.current.userIsLoggedIn) {
      this.userService.updateUsersLanguage(lang);
    } */
  }

  public useUsersLanguage(lang: string): void {
    if (lang.startsWith('de')) {
      lang = 'de-DE';
    }

    if (lang.startsWith('en')) {
      lang = 'en-GB';
    }

    this.translate.use(lang);
  }

  public transformDate(date: any): string {
    if (date) {
      return this.datePipe.transform(date.seconds * 1000);
    }
    return '';
  }

  private onLangChange(): void {
    // tslint:disable-next-line: no-console
    console.log(
      'Received language change. Change to: ' + this.translate.currentLang
    );
    this.adapter.setLocale(this.translate.currentLang);
    this.datePipe = new DatePipe(this.translate.currentLang);
    this.translate.get('app.popup.public_ok_button').subscribe((res) => {
      this.confirmButton = res;
    });
    this.current.selectedCountry = this.translate.currentLang;
  }

  private getFullIsoCode(browserLang: string): string {
    return this.translate.langs.find((isoCode) =>
      isoCode.startsWith(browserLang)
    );
  }
}
