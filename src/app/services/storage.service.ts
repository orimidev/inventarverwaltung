import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { BaseService } from '../components/shared/base.service';
import { FurnitureGroup } from '../models/furnitureGroup.model';
import { StorageModel } from '../models/storage.model';

@Injectable({
  providedIn: 'root',
})
export class StorageRoomService extends BaseService {
  public storages: StorageModel[] = [];
  public storages$ = new BehaviorSubject<StorageModel[]>([]);
  private readonly CK_STORAGE = 'storage';
  private readonly CK_FURNITEGROUP = 'furniture';

  constructor(private afs: AngularFirestore) {
    super();
    this.subscribeToStoragesFromFirestore();
  }

  public async add(data: StorageModel): Promise<DocumentReference> {
    return this.afs.collection(this.CK_STORAGE).add(data);
  }

  public async delete(
    data: StorageModel,
    allFurniture: FurnitureGroup[]
  ): Promise<void> {
    this.addOrDeleteNewStorageToAllFurnitures(allFurniture, data.id, false);
    return this.afs.doc(`${this.CK_STORAGE}/${data.id}`).delete();
  }

  public addOrDeleteNewStorageToAllFurnitures(
    allFurnitureGroups: FurnitureGroup[],
    newStorageId: string,
    create: boolean
  ) {
    allFurnitureGroups.forEach((d) => {
      this.updateStoragesFromFurnitureGroup(d, newStorageId, create);
    });
  }

  private updateStoragesFromFurnitureGroup(
    data: FurnitureGroup,
    newStorageId: string,
    create: boolean
  ) {
    const currentStorages = data.storages;
    this.storages.forEach((item) => {
      if (newStorageId === item.id && create) {
        currentStorages.push({ ...item, stock: 0 });
      } else if (newStorageId === item.id && !create) {
        data.storages.forEach((d, index) => {
          if (d.id === item.id) {
            currentStorages.splice(index, 1);
          }
        });
      }
    });

    return this.afs
      .doc(`${this.CK_FURNITEGROUP}/${data.furnitureGroupId}`)
      .update({ storages: currentStorages });
  }

  public update(store: StorageModel): Promise<void> {
    return this.afs.doc(this.CK_STORAGE + '/' + store.id).update({ ...store });
  }

  public addIdToStore(docId: string): Promise<void> {
    return this.afs.doc(this.CK_STORAGE + '/' + docId).update({ id: docId });
  }

  private subscribeToStoragesFromFirestore() {
    this.afs
      .collection<StorageModel>(this.CK_STORAGE)
      .snapshotChanges()
      .subscribe((response) => {
        const dataFromResponse = response.map((e) => {
          return {
            ...e.payload.doc.data(),
          } as StorageModel;
        });
        this.storages = dataFromResponse;
        this.storages$.next(this.storages);
      });
  }
}
