import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { isNullOrUndefined } from 'util';
import { BaseService } from '../components/shared/base.service';
import { FurnitureGroup } from '../models/furnitureGroup.model';
import { ImageModel } from '../models/image.model';

@Injectable({
  providedIn: 'root',
})
export class FurnitureGroupService extends BaseService {
  public furnitures: FurnitureGroup[];
  public furnitures$ = new BehaviorSubject<FurnitureGroup[]>([]);
  private downloadUrls: ImageModel[] = [];
  private readonly CK_FURNITEGROUP = 'furniture';
  private readonly SK_IMAGE = 'furniture';

  constructor(
    private afs: AngularFirestore,
    private storage: AngularFireStorage
  ) {
    super();
    this.subscribeToFurnitureGroupsFromFirestore();
  }

  public add(data: FurnitureGroup): Promise<DocumentReference> {
    return this.afs.collection(this.CK_FURNITEGROUP).add(data);
  }

  public update(group: FurnitureGroup): Promise<void> {
    return this.afs
      .doc(`${this.CK_FURNITEGROUP}/${group.furnitureGroupId}`)
      .update({ ...group });
  }

  public updateStock(
    group: FurnitureGroup,
    storageId: string,
    newStock: number
  ): Promise<void> {
    const newGroup = group;
    const indexOfGroupToUpdate = newGroup.storages.findIndex(
      (x) => x.id === storageId
    );
    newGroup.storages[indexOfGroupToUpdate].stock = newStock;
    return this.update(newGroup);
  }

  public getFurnitureImage(path: string): Promise<string> {
    return this.storage
      .ref(`${this.SK_IMAGE}/${path}`)
      .getDownloadURL()
      .toPromise();
  }

  public getGroup(docId: string) {
    return this.furnitures.find((x) => x.furnitureGroupId === docId);
  }

  public getTotalStockCount(item: FurnitureGroup) {
    let count = 0;
    for (const storage of item.storages) {
      count += storage.stock;
    }
    return count;
  }

  public getStockCount(item: FurnitureGroup, storageId: string) {
    const index = item.storages.findIndex((x) => storageId === x.id);
    return item.storages[index].stock;
  }

  public getTotalStockCountForStorage(storageId: string) {
    let count = 0;
    for (const furniture of this.furnitures) {
      const index = furniture.storages.findIndex((x) => storageId === x.id);
      count += furniture.storages[index].stock;
    }
    return count;
  }

  public delete(data: FurnitureGroup): Promise<void> {
    return this.afs
      .collection(this.CK_FURNITEGROUP)
      .doc(data.furnitureGroupId)
      .delete();
  }

  public addIdToFurnitureGroup(docId: string): Promise<void> {
    return this.afs
      .doc(this.CK_FURNITEGROUP + '/' + docId)
      .update({ furnitureGroupId: docId });
  }

  private async subscribeToFurnitureGroupsFromFirestore() {
    this.afs
      .collection<FurnitureGroup>(this.CK_FURNITEGROUP)
      .snapshotChanges()
      .subscribe(async (response) => {
        const dataFromResponse = response.map((e) => {
          return {
            ...e.payload.doc.data(),
          } as FurnitureGroup;
        });

        this.furnitures = await this.downloadAndSetAllImages(
          dataFromResponse.sort(this.compare)
        );
        this.furnitures$.next(this.furnitures);
      });
  }

  public async getImageDownloadURL(object: FurnitureGroup) {
    return new Promise<any>((resolve, reject) => {
      const downloadUrl = this.downloadUrls.find(
        (x) => x.key === object.furnitureGroupId
      );

      if (!isNullOrUndefined(downloadUrl)) {
        resolve(downloadUrl.path);
      } else {
        this.getFurnitureImage(object.imgPath)
          .then((image) => {
            const imageToLocallySave: ImageModel = {
              key: object.furnitureGroupId,
              originalPath: object.imgPath,
              path: image,
            };

            if (
              isNullOrUndefined(
                this.downloadUrls.find((x) => x === imageToLocallySave)
              )
            ) {
              this.downloadUrls.push(imageToLocallySave);
            }

            resolve(image);
          })
          .catch((e) => {
            console.log(e.code);
          });
      }
    });
  }

  private async downloadAndSetAllImages(furnitures: FurnitureGroup[]) {
    // tslint:disable-next-line: no-shadowed-variable
    for (const { group, index } of furnitures.map((group, index) => ({
      group,
      index,
    }))) {
      if (!group.imgPath) {
        continue;
      } else {
        group.imageDownloadPath = await this.getImageDownloadURL(group);
      }
    }

    return furnitures;
  }
}
