import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class CurrentStateService {
  public loading = true;
  public selectedCountry = '';

  public startedUserFlowTest = false;

  //  public allUser: UserModel[] = [];

  constructor() {}
}
