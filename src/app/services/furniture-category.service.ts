import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { isNullOrUndefined } from 'util';
import { BaseService } from '../components/shared/base.service';
import { FurnitureCategory } from '../models/furnitureCategory.model';
import { ImageModel } from '../models/image.model';

@Injectable({
  providedIn: 'root',
})
export class FurnitureCategoryService extends BaseService {
  public categories: FurnitureCategory[];
  public categories$ = new BehaviorSubject<FurnitureCategory[]>(null);
  private readonly CK_CATEGORIES = 'furnitureCategory';
  private readonly CK_IMAGE = 'furniture';
  private downloadUrls: ImageModel[] = [];

  constructor(
    private afs: AngularFirestore,
    private storage: AngularFireStorage
  ) {
    super();
    this.subscribeToFurnitureCategoryFromFirestore();
  }

  public add(data: FurnitureCategory): Promise<DocumentReference> {
    return this.afs.collection(this.CK_CATEGORIES).add(data);
  }

  public delete(data: FurnitureCategory): Promise<void> {
    return this.afs
      .collection(this.CK_CATEGORIES)
      .doc(data.furnitureCategoryId)
      .delete();
  }

  public update(cat: FurnitureCategory): Promise<void> {
    return this.afs
      .doc(this.CK_CATEGORIES + '/' + cat.furnitureCategoryId)
      .update({ ...cat });
  }

  public getImage(path: string): Promise<string> {
    return this.storage
      .ref(`${this.CK_IMAGE}/${path}`)
      .getDownloadURL()
      .toPromise();
  }

  public addIdToFurnitureCategory(docId: string): Promise<void> {
    return this.afs
      .doc(this.CK_CATEGORIES + '/' + docId)
      .update({ furnitureCategoryId: docId });
  }

  private subscribeToFurnitureCategoryFromFirestore() {
    this.afs
      .collection<FurnitureCategory>(this.CK_CATEGORIES)
      .snapshotChanges()
      .subscribe(async (response) => {
        const categoriesFromResponse = response.map((e) => {
          return {
            ...e.payload.doc.data(),
          } as FurnitureCategory;
        });

        this.categories = await this.downloadAndSetAllImages(
          categoriesFromResponse.sort(this.compare)
        );
        this.categories$.next(this.categories);
      });
  }

  public async getImageDownloadURL(object: FurnitureCategory) {
    return new Promise<any>((resolve, reject) => {
      const downloadUrl = this.downloadUrls.find(
        (x) => x.key === object.furnitureCategoryId
      );

      if (!isNullOrUndefined(downloadUrl)) {
        resolve(downloadUrl.path);
      } else {
        this.getImage(object.imgPath)
          .then((image) => {
            const imageToLocallySave: ImageModel = {
              key: object.furnitureCategoryId,
              originalPath: object.imgPath,
              path: image,
            };

            if (
              isNullOrUndefined(
                this.downloadUrls.find((x) => x === imageToLocallySave)
              )
            ) {
              this.downloadUrls.push(imageToLocallySave);
            }

            resolve(image);
          })
          .catch((e) => {
            console.log(e.code);
          });
      }
    });
  }

  private async downloadAndSetAllImages(categories: FurnitureCategory[]) {
    // tslint:disable-next-line: no-shadowed-variable
    for (const { cat, index } of categories.map((cat, index) => ({
      cat,
      index,
    }))) {
      if (!cat.imgPath) {
        continue;
      } else {
        cat.imageDownloadPath = await this.getImageDownloadURL(cat);
      }
    }

    return categories;
  }
}
