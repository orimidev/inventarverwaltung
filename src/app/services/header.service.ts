import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class HeaderService {
  public navBarOpend = false;
  public title = 'Lagerfuchs';
  public unsavedChanges: boolean;

  constructor() {}

  // TODO PREVENT USAVE CHANGES
}
